import axios from "axios";

export const axiosInstance = axios.create({
  baseURL: process.env.REACT_APP_BASE_URL || "https://api.warung.io/",
  headers: {
    "x-tenant-id":
      process.env.REACT_APP_TENANT_ID || "623978ecf39f6593fdd35335",
  },
});
