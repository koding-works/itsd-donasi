import React from "react";
import { Grid, Typography } from "@material-ui/core";
import ArrowBackIcon from "@material-ui/icons/ArrowBack";
import { withStyles } from "@material-ui/core/styles";
import FormControlLabel from "@material-ui/core/FormControlLabel";
import Checkbox from "@material-ui/core/Checkbox";

const GreenCheckbox = withStyles({
  root: {
    color: "#4D4D4D",
    "&$checked": {
      color: "#2DBE78",
    },
  },
  checked: {},
})((props) => <Checkbox color="default" {...props} />);

function DonationChoice(props) {
  const defaultValue = {
    INFAK: false,
    ZAKAT: false,
  };
  const [state, setState] = React.useState({
    ...defaultValue,
  });
  const handleChange = (event) => {
    setState({
      ...defaultValue,
      [event.target.name]: event.target.checked,
    });
  };

  const handleClick = (dataName, dataValue) => {
    if (!dataValue) {
      props.handleSelectedType(null);
      props.setDisplayCard();
    } else {
      props.handleSelectedType(dataName);
      props.setDisplayCard();
    }
    setState({
      ...defaultValue,
      [dataName]: dataValue,
    });
  };

  React.useEffect(() => {
    setState({
      ...state,
      [props.selectedPayment]: true,
    });
  }, []);

  return (
    <Grid style={{ height: 598 }}>
      <Grid
        onClick={() => props.setDisplayCard()}
        style={{ display: "flex", alignItems: "center", cursor: "pointer" }}
      >
        <ArrowBackIcon />
        <Typography style={{ fontSize: 14, fontWeight: 700, marginLeft: 8 }}>
          Kembali
        </Typography>
      </Grid>

      <Grid
        style={{
          marginTop: 12,
          padding: "6px 11px",
          background: "#F2F3F4",
          width: "100%",
          borderRadius: 2,
        }}
      >
        <Typography style={{ fontSize: 12, fontWeight: 700, color: "#4D4D4D" }}>
          Pilihan Donasi
        </Typography>
      </Grid>
      <Grid style={{ padding: "0px 12px" }}>
        <Grid
          onClick={() => {
            handleClick("INFAK", !state.INFAK);
          }}
          style={{
            display: "flex",
            width: "100%",
            padding: "18px 0px",
            alignItems: "center",
            borderBottom: "1px solid #CCCCCC",
          }}
        >
          <Grid item xs={6}>
            <Typography
              style={{
                fontSize: 16,
                fontWeight: 400,
                textTransform: "uppercase",
              }}
            >
              infak
            </Typography>
          </Grid>
          <Grid
            item
            xs={6}
            style={{ justifyContent: "flex-end", display: "flex" }}
          >
            <FormControlLabel
              style={{ margin: 0 }}
              control={
                <GreenCheckbox
                  checked={state.INFAK}
                  onChange={handleChange}
                  name="INFAK"
                  fontSize="small"
                  style={{ padding: 0 }}
                />
              }
            />
          </Grid>
        </Grid>
        <Grid
          onClick={() => {
            handleClick("ZAKAT", !state.ZAKAT);
          }}
          style={{
            display: "flex",
            width: "100%",
            padding: "18px 0px",
            alignItems: "center",
            borderBottom: "1px solid #CCCCCC",
          }}
        >
          <Grid item xs={6}>
            <Typography
              style={{
                fontSize: 16,
                fontWeight: 400,
                textTransform: "uppercase",
              }}
            >
              zakat
            </Typography>
          </Grid>
          <Grid
            item
            xs={6}
            style={{ justifyContent: "flex-end", display: "flex" }}
          >
            <FormControlLabel
              style={{ margin: 0 }}
              control={
                <GreenCheckbox
                  checked={state.ZAKAT}
                  onChange={handleChange}
                  name="ZAKAT"
                  fontSize="small"
                  style={{ padding: 0 }}
                />
              }
            />
          </Grid>
        </Grid>
      </Grid>
    </Grid>
  );
}

export default DonationChoice;
