import { Grid, Typography } from "@material-ui/core";
import React from "react";
import { useMediaQuery } from "react-responsive";
import { useRouter } from "next/router";
import CardCampaign from "../components/CardCampaign";

function BannerSectionTwo(props) {
  const router = useRouter();
  const [isMobile, setIsMobile] = React.useState(false);
  const isMob = useMediaQuery({ maxWidth: 1024 });
  const xs = useMediaQuery({ maxWidth: 650 });

  React.useEffect(() => {
    if (isMob) {
      setIsMobile(isMob);
    } else {
      setIsMobile(false);
    }
  }, [isMob]);


  return (
    <>
      {isMob ? (
        <>
          <>
            <Grid
              style={{
                margin: "30px 20px",
              }}
            >
            
            </Grid>

            <div style={{ padding: xs ? "0px 16px" : "0px 32px" }}>
              {props?.campaigns?.length > 0 && (
                <div
                  style={{
                    display: "flex",
                    justifyContent: "space-between",
                    marginBottom: 25,
                    alignItems: "center",
                  }}
                >
                  <div
                    style={{
                      fontSize: xs ? 16 : 28,
                      fontWeight: 600,
                      color: "#4d4d4d",
                    }}
                  >
                    Campaign Unggulan
                  </div>
                  <div
                    style={{
                      fontSize: xs ? 10 : 16,
                      fontWeight: 700,
                      color: "#28A96B",
                      cursor: "pointer",
                    }}
                    onClick={() => router.push("/campaigns")}
                  >
                    Lihat semua
                  </div>
                </div>
              )}

              <div style={{ margin: xs ? "0px 16px" : "0px 30px" }}>
                <Grid container spacing={3}>
                  {props?.campaigns?.slice(0, 6)?.map((data) => (
                    <Grid
                      item
                      xs={xs ? 12 : 6}
                    >
                      <CardCampaign data={data} />
                    </Grid>
                  ))}
                </Grid>
              </div>
            </div>


          
          </>
        </>
      ) : (
        <>
          <>
            <Grid
              style={{
                margin: "33px 0px",
                marginLeft: 106,
              }}
            >
         
            </Grid>

            <div style={{ margin: "0px 100px" }}>
              {props?.campaigns?.length > 0 && (
                <div
                  style={{
                    display: "flex",
                    justifyContent: "space-between",
                    marginBottom: 25,
                  }}
                >
                  <div
                    style={{ fontSize: 28, fontWeight: 600, color: "#4d4d4d" }}
                  >
                    Campaign Unggulan
                  </div>
                  <div
                    style={{
                      fontSize: 16,
                      fontWeight: 700,
                      color: "#28A96B",
                      cursor: "pointer",
                    }}
                    onClick={() => router.push("/campaigns")}
                  >
                    Lihat semua
                  </div>
                </div>
              )}

              <div style={{ margin: "0px 30px" }}>
                <Grid container spacing={3}>
                  {props?.campaigns?.slice(0, 6)?.map((data) => (
                    <Grid
                      item
                      xs={4}
                    >
                      <CardCampaign data={data} />
                    </Grid>
                  ))}
                </Grid>
              </div>
            </div>
          

          </>
        </>
      )}
    </>
  );
}

export default BannerSectionTwo;
